# MQTT Test for BusEmissions Project
# Jon Sowman 2015
# University of Southampton

import time
import paho.mqtt.publish as publish

while(1):
    publish.single("stu/001/test", "hello world", hostname="localhost")
    time.sleep(10)
